import unittest
from Refactored_CMC.EnemyManager import EnemyManager
from Refactored_CMC.Path import Path

class EnemyManagerTest(unittest.TestCase):
    def test_first_placement(self):
        a_path = Path()
        my_enemy_manager = EnemyManager(['1', '2', '1 2 3'])
        my_enemy_manager.spawn_enemy(round_number=2)
        self.assertEqual(my_enemy_manager[a_path[0]].health, 1)

    def test_first_step(self):
        a_path = Path()
        my_enemy_manager = EnemyManager(['1', '2', '1 2 3'])
        my_enemy_manager.spawn_enemy(round_number=2)
        my_enemy_manager.move_all()
        self.assertEqual(my_enemy_manager[a_path[0]] is None
                         and my_enemy_manager[a_path[1]].health == 1, True)

    def test_second_placement(self):
        a_path = Path()
        my_enemy_manager = EnemyManager(['1', '2', '1 2 3'])
        my_enemy_manager.spawn_enemy(round_number=2)
        my_enemy_manager.move_all()
        my_enemy_manager.spawn_enemy(round_number=2)
        self.assertEqual(my_enemy_manager[a_path[0]].health == 2
                         and my_enemy_manager[a_path[1]].health == 1, True)

    def test_second_step(self):
        a_path = Path()
        my_enemy_manager = EnemyManager(['1', '2', '1 2 3'])
        my_enemy_manager.spawn_enemy(round_number=2)
        my_enemy_manager.move_all()
        my_enemy_manager.spawn_enemy(round_number=2)
        my_enemy_manager.move_all()
        self.assertEqual(my_enemy_manager[a_path[0]] is None
                         and my_enemy_manager[a_path[1]].health == 2
                         and my_enemy_manager[a_path[2]].health == 1, True)


if __name__ == '__main__':
    unittest.main()