import unittest
import numpy as np
from Refactored_CMC.TowerPlacer import TowerPlacer
from Refactored_CMC.EnemyManager import EnemyManager
from collections import deque

class TowerPlacerTest(unittest.TestCase):
    def test_tower_placer_creation(self):
        np.random.seed(0)
        p_matrix = np.zeros((10, 10, 2))
        p_matrix[:,:,0] = np.eye(10)/10
        p_matrix[:,:,1] = np.eye(10)/10
        my_tower_placer = TowerPlacer(p_matrix)
        is_equal = np.all(my_tower_placer.p_matrix == p_matrix)
        my_enemy_manager = EnemyManager(['1', '2', '1 2 3'])
        my_tower_manager = my_tower_placer.place_towers(my_enemy_manager)
        is_equal_manager = my_tower_manager.enemy_manager.round_data == my_enemy_manager.round_data
        is_tower_dict_right_length = len(my_tower_manager.tower_dict) == 2
        return self.assertEqual(True, is_equal and is_equal_manager and is_tower_dict_right_length)


if __name__ == '__main__':
    unittest.main()