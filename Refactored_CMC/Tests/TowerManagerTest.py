import unittest
from Refactored_CMC.EnemyManager import EnemyManager
from Refactored_CMC.Tower import Tower
from Refactored_CMC.TowerManager import TowerManager
from Refactored_CMC.Path import Path

class TowerManagerTest(unittest.TestCase):
    def test_fire_all(self):
        a_path = Path()
        start = a_path[0]
        tower_location = (start[0], start[1] + 1)
        an_enemy_manager = EnemyManager(round_data=['1', '2', '1 2 3'])
        an_enemy_manager.spawn_enemy(2)
        a_tower_dict = {tower_location: Tower()}
        a_tower_manager = TowerManager(tower_dict=a_tower_dict, enemy_manager=an_enemy_manager)
        a_tower_manager.fire_towers()
        self.assertEqual(True, True)

if __name__ == '__main__':
    unittest.main()