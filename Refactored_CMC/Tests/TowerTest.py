import unittest
from Refactored_CMC.Tower import Tower

class TowerTest(unittest.TestCase):
    def test_firing(self):
        a_tower = Tower(damage=2, firing_range=3, reload_time=1)
        a_tower.fire()
        self.assertEqual(a_tower.turns_to_wait, 1)