class Tower(object):
    def __init__(self, damage=1, firing_range=2, reload_time=0):
        self.damage = damage
        self.firing_range = firing_range
        self.reload_time = reload_time
        self.turns_to_wait = 0

    def fire(self):
        if self.turns_to_wait == 0:
            self.turns_to_wait = self.reload_time
            return True
        else:
            self.turns_to_wait -= 1
            return False