from Refactored_CMC.TowerManager import TowerManager
from Refactored_CMC.Tower import Tower
from Refactored_CMC.Path import Path
import numpy as np

class TowerPlacer(object):
    def __init__(self, p_matrix):
        self.path = Path()
        self.p_matrix = p_matrix
        shape = p_matrix.shape
        print(len(shape))
        self.N = shape[0]
        self.N_towers = shape[2]

    def __choose_coordinate(self, i):
        """Chooses the coordinate to place a tower based on the p_coordinates matrix"""
        cum_matrix = self.p_matrix[:, :, i]
        cum_list = np.reshape(cum_matrix, (self.N**2, 1)).copy()
        cum_list = np.cumsum(cum_list)
        random_number = np.random.rand()
        index_cum_list = (cum_list < random_number).sum()
        coordinate_chosen = (index_cum_list // self.N, index_cum_list % self.N)
        return coordinate_chosen

    def place_towers(self, enemy_manager):
        """Generates a TowerManager with the proper tower_dict"""
        tower_dict = {}
        for i in range(self.N_towers):
            while True:
                (x, y) = self.__choose_coordinate(i)
                if (x, y) not in tower_dict:
                    break
            tower_dict[(x, y)] = Tower()
        return TowerManager(tower_dict, enemy_manager)


# np.random.seed(0)
# p_matrix = np.zeros((10, 10, 2))
# p_matrix[:,:,0] = np.eye(10)/10
# p_matrix[:,:,1] = np.eye(10)/10
# print(p_matrix.shape[2])
# my_tower_placer = TowerPlacer(p_matrix)