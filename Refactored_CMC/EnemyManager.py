from Refactored_CMC.Path import Path
from Refactored_CMC.Enemy import Enemy
from collections import deque

class EnemyManager(object):
    def __init__(self, round_data: list):
        self.enemy_dict = {}
        for i in Path():
            self.enemy_dict[i] = None
        self.round_data = []
        for i in round_data:
            self.round_data.append(deque(i.split(" ")))
        print(round_data)

    def __str__(self):
        l = []
        for k in self.enemy_dict:
            if self.enemy_dict[k] is not None:
                l.append((k, self.enemy_dict[k].health))
        formated = []
        for i in l:
            formated.append("Enemy at {} with health {} \n".format(i[0], i[1]))
        s = "".join(formated)
        return s


    def __getitem__(self, item):
        return self.enemy_dict[item]

    def spawn_enemy(self, round_number):
        if len(self.round_data[round_number]) == 0:
            raise IndexError
        self.enemy_dict[Path().first()] = Enemy(self, int(self.round_data[round_number].popleft()))
        return True

    def move_all(self):
        enemy_data_sorted = []
        for i in self.enemy_dict.copy().values():
            if i is not None:
                enemy_data_sorted.append(i)
        enemy_data_sorted.sort()
        for enemy in enemy_data_sorted:
            enemy.move()