from Refactored_CMC.Path import Path

class Enemy(object):
    """Enemy position is encoded in the keys of enemy_list in EnemyManager"""
    def __init__(self, parent_list, health):
        self.path = Path()
        self.parent_list = parent_list
        self.health = health

    def move(self):
        old_position = self.path.pop()
        #print("Old_position: " + str(old_position))
        new_position = self.path.first()
        #print("New position: " + str(new_position))
        self.parent_list.enemy_dict[(new_position[0], new_position[1])] = self
        self.parent_list.enemy_dict[(old_position[0], old_position[1])] = None

    def __str__(self):
        return "I am an enemy with: \n Path: {} \n ParentList: {} \n Health: {}".format(self.path, self.parent_list, self.health)

    def __lt__(self, other):
        if other is None:
            return False
        if len(self.path) < len(other.path):
            return True
        return False