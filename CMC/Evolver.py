import numpy as np
from more_itertools import unique_everseen
from collections import deque

width_mutability= 1
depth_mutability_mean = 0.1
depth_mutability_sd = 0.02
mutability_noise = 0.01
winners_reproducing = 19
losers_reproducing = 1

def choose_fittest(generation_result):
    """Takes (fitness, P_matrix) and returns winners_reproducing + losers_reproducing p_matrices"""
    np.random.shuffle(generation_result)
    my_results = sorted(generation_result, key=lambda x: x[0], reverse=True)
    fittest = my_results[:winners_reproducing]
    losers = my_results[winners_reproducing:]
    np.random.shuffle(losers)
    for i in range(losers_reproducing):
        fittest.append(losers.pop())
    return fittest

def add_noise(child):
    """Adds noise to a child based on the mutabilities"""
    for tower in range(child.shape[-1]):
        random_number = np.random.choice(np.arange(0, child[..., tower].size), width_mutability, replace=False)
        random_indices_to_mutate = np.vstack((random_number // len(child[..., tower]), random_number % len(child[..., tower])))
        for column, row in random_indices_to_mutate.T:
            index = (column, row, tower)
            child[index] += depth_mutability_mean + abs(np.random.normal(0, depth_mutability_sd))
        child /= child.sum()

def produce_child(parents):
    father = parents[0]
    mother = parents[1]
    child = (mother + father)/2
    add_noise(child)
    return child

def normalize(matrix):
    for i in range(matrix.shape[-1]):
        matrix[..., i] /= matrix[..., i].sum()
    return matrix

def mate(fittest, N_individuals):
    fittest_genes = [i[1] for i in fittest]
    mating_indices = [np.random.choice(len(fittest), 2, replace=False) for i in range(N_individuals)]
    mating_pairs = [(fittest_genes[i[0]], fittest_genes[i[1]]) for i in mating_indices]
    new_generation = [produce_child(pair) for pair in mating_pairs]
    map(add_noise, new_generation)
    newer_generation = []
    for i in new_generation:
        newer_generation.append(normalize(i))
    return new_generation
