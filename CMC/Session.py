import os
import time
from multiprocessing import Pool
import pickle

import matplotlib.pyplot as plt
import numpy as np
from scipy.spatial.distance import cityblock as manhattan_distance

from CMC.Evolver import choose_fittest, mate
from CMC.Enemy import EnemyList
from CMC.Field import Field
from CMC.Path import Path
from CMC.Tower import TowerList

os.system("taskset -p 0xff %d" % os.getpid())


N_individuals = 400
N_squares = 10
N_generations = 10
N_rounds = 28
N_towers = 5
path = "/home/romano/Desktop/results.txt"

class Session(object):
    def __init__(self, N, p_coordinates = None):
        self.N = N
        self.field = Field(N)
        self.towers = TowerList(N)
        if p_coordinates is None:
            init_matrix = np.random.rand(N, N, N_towers)
            for i in range(N_towers):
                init_matrix[:, :, i] /= init_matrix[:, :, i].sum()
            self.p_coordinates = init_matrix
            self.memory_p_coordinates = init_matrix.copy()
        else:
            self.p_coordinates = p_coordinates
            self.memory_p_coordinates = p_coordinates.copy()
        self.enemy_list = EnemyList(N)
        self.life_remaining = 10

    #Tower selection#
    def choose_coordinate(self, i):
        """Chooses the coordinate to place a tower based on the p_coordinates matrix"""
        cum_matrix = self.p_coordinates[:, :, i]
        cum_list = np.reshape(cum_matrix, (self.N**2, 1)).copy()
        cum_list = np.cumsum(cum_list)
        random_number = np.random.rand()
        index_cum_list = (cum_list < random_number).sum()
        coordinate_chosen = (index_cum_list // self.N, index_cum_list % self.N)
        return coordinate_chosen

    def is_space_for_tower(self, i, j):
        """Adds a tower if there is space and returns true, otherwise returns false"""
        if self.towers.is_empty_space(i, j):
            self.towers.add_tower(i, j)
            return True
        return False

    def place_tower(self):
        """Places all towers"""
        for i in range(N_towers):
            tower_placed_flag = False
            while not tower_placed_flag:
                (x, y) = self.choose_coordinate(i)
                tower_placed_flag = self.is_space_for_tower(x, y)

    #Tower fire#
    def fire_towers(self):
        """Fires all towers"""
        for key, tower in self.towers.towers.items():
            coordinate_tower = key
            targets = []
            for enemy_key, enemy in self.enemy_list.enemies.items():
                if manhattan_distance(coordinate_tower, enemy_key) <= tower.range:
                    targets.append(enemy)
            if len(targets) != 0: # makes the tower fire at the enemy closest to the exit
                targets.sort()
                targets[0].health -= tower.damage

    #Enemy regulation#
    def spawn_enemies(self, health):
        """Places a new enemy at the first point of Path"""
        self.enemy_list.spawn_enemy(health=health)

    def move_enemies(self):
        """Moves all enemies"""
        self.enemy_list.move_all()

    #Damage check#
    def get_damage(self):
        """Checks if an enemy has reached the end. If so, remove the enemy from the map and deal
        one damage"""
        final_point = Path().final()
        for key, enemy in self.enemy_list.enemies.items():
            if enemy.position == final_point:
                break
        else:
            return
        del(self.enemy_list.enemies[final_point])
        self.enemy_list.filled_matrix[final_point[0], final_point[1]] = False
        self.life_remaining -= 1

    def update(self):
        self.move_enemies()
        self.fire_towers()
        self.get_damage()

    def round(self, round_info):
        my_round_info = round_info.split(' ') # Interpret round info/enemy list
        for char in my_round_info: # Update when the enemies spawn
            if char != '-':
                self.spawn_enemies(int(char))
            self.update()
            if self.life_remaining <= 0:
                return False
        while len(self.enemy_list.enemies) != 0: # If all enemies have spawned, keep running until no more enemies
            self.update()
            if self.life_remaining <= 0:
                return False
        return True

    def __str__(self):
        a = []
        for i, x in enumerate(self.towers.filled_matrix):
            for j, y in enumerate(x):
                if y:
                    a.append('t')
                elif self.enemy_list.filled_matrix[i, j]:
                    a.append('e')
                else:
                    a.append(self.field.floor[i][j].icon)
            a.append('\n')
        return_string = ''.join(a)
        return return_string

def run_individual(iterable):
    """The behavior shown by one individual, returning a tuple of (fitness, P_matrix)"""
    proc_num = iterable[1]
    next_generation = iterable[0]
    rounds = [
        '1', #0
        '2',
        '4',
        '6',
        '8',
        '10',#5
        '6 6 6',
        '8 8 8',
        '10 10 10',
        '14',
        '14 14 14', #10
        '18 18 18',
        '20 20',
        '22 22',
        '24 24',
        '28 28 28', #15
        '30 30 30',
        '40',
        '32 32 32',
        '34 34 34',
        '38 38 38',
        '40 40 40',
        '50',
        '42 42 42',
        '42 42 42 42 42',
        '55',
        '46 46 46',
        '50 50 50',
        '99 99 99 99 99 99 99 99 99 99 99 99'
    ]
    np.random.seed(proc_num)
    #print(next_generation)
    for i in range(N_towers):
        if next_generation is None:
            break
    s = Session(N_squares, next_generation)
    for i in range(N_towers):
        s.place_tower()
    i = 0
    for i, my_round in enumerate(rounds):
        flag = s.round(my_round)
        if not flag:
            break
    return i, s.memory_p_coordinates

def run_generation(new_generation=None):
    if new_generation is None:
        new_generation = [None for _ in range(N_individuals)]
    with Pool(N_individuals) as p:
        my_iterable = [j for j in zip(new_generation,
                                      [i for i in np.random.randint(0, 999999, (N_individuals, 1))])]
        ans = p.map(run_individual, my_iterable)
    scores = [i[0] for i in ans]
    a = 0
    for i in new_generation:
        if i is None: break
        a += (np.mean(i[3,:]) + 2*np.mean(i[4,:]) + 2*np.mean(i[6,:]) + np.mean(i[7,:]))
    plt.hist(scores, bins=np.arange(0.5, 28.5, 1))
    plt.title("Distribution of fitness scores")
    plt.xlabel("Fitness score")
    plt.ylabel("#individuals")
    print((np.mean(scores), np.std(scores)))
    print("GENERATION DONE")
    return ans


def parallel_main():
    v = []
    a = run_generation()
    b = choose_fittest(a)
    c = mate(b, N_individuals)
    run_generation(c)
    return None

if __name__ == '__main__':
    for _ in range(10):
        print("Run done")
        n = parallel_main()
