class Tower(object):
    def __init__(self, name: str, firing_range: int, damage: int, cooldown:int):
        self.name = name
        self.firing_range = firing_range
        self.damage = damage
        self.cooldown = cooldown
        self.current_cooldown = 0