from KISS.Round import Round
from KISS.Path import path

class Game(object):
    def __init__(self, towers_to_place, enemy_waiting_list):
        self.towers_to_place = towers_to_place # [((x, y), Tower), ...]
        self.enemies_waiting_list_foreach_round = enemy_waiting_list # [deque[Enemy, Enemy ... ], deque[Enemy, ... ]]
        self.life = 20
        self.current_round = Round(path, self.enemies_waiting_list_foreach_round.pop(0))

    def place_tower(self, tower_placed):
        self.current_round.field.add_tower(*tower_placed)

    def run(self):
        level_counter = 1
        while True:
            # In a round:
            # * Place new tower if possible based on towers_to_place
            # * Run round
            # * Replace empty list of enemies by front element of enemy_waiting_list_foreach_round
            if len(self.towers_to_place) != 0:
                self.place_tower(self.towers_to_place.pop(0))
            self.current_round.run_round()
            self.life -= self.current_round.pass_damage_dealt()
            if self.life < 0 or len(self.enemies_waiting_list_foreach_round) == 0:
                print(len(self.enemies_waiting_list_foreach_round))
                return level_counter + 1
            self.current_round.enemy_waiting_list = self.enemies_waiting_list_foreach_round.pop(0)
            level_counter += 1

