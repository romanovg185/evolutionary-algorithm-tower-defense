from KISS.Field import Field
from KISS.Enemy import Enemy
from KISS.Tower import Tower
from KISS.Round import Round
import unittest
from collections import deque

path = [(5, 0), (5, 1), (5, 2), (5, 3), (5, 4)]

class RunTest(unittest.TestCase):
    ## Test suite movement
    def test_movement_one_step(self):
        my_game = Round(path)
        my_enemy = Enemy('bob', health=1, movement=1)
        my_game.field.add_enemy(my_enemy)
        my_game.move_enemies()
        self.assertEqual(my_game.field[(5,1)].name, 'bob')

    def test_movement_multiple_steps(self):
        my_game = Round(path)
        my_enemy = Enemy('bob', health=1, movement=2)
        my_game.field.add_enemy(my_enemy)
        my_game.move_enemies()
        self.assertEqual(my_game.field[(5,2)].name, 'bob')

    def test_movement_off_map(self):
        my_game = Round(path)
        my_enemy = Enemy('bob', health=1, movement=1)
        my_game.field.add_enemy(my_enemy)
        my_game.move_enemies()
        my_game.move_enemies()
        my_game.move_enemies()
        my_game.move_enemies()
        self.assertEqual(my_game.move_enemies(), True)

    def test_movement_off_map_multiple_steps(self):
        my_game = Round(path)
        my_enemy = Enemy('bob', health=1, movement=2)
        my_game.field.add_enemy(my_enemy)
        my_game.move_enemies()
        my_game.move_enemies()
        self.assertEqual(my_game.move_enemies(), True)

    ## Test suite gunfire
    def test_firing_single(self):
        my_game = Round(path)
        my_tower = Tower('john', firing_range=1, damage=1, cooldown=1)
        my_game.field.add_tower(position=(4, 0), tower=my_tower)
        my_enemy = Enemy('bob', health=2, movement=1)
        my_game.field.add_enemy(my_enemy)
        my_game.fire_towers()
        enemies = my_game.field.get_enemies()
        self.assertEqual(enemies[0][1].health, 1)

    def test_firing_to_kill(self):
        my_game = Round(path)
        my_tower = Tower('john', firing_range=1, damage=2, cooldown=1)
        my_game.field.add_tower(position=(4, 0), tower=my_tower)
        my_enemy = Enemy('bob', health=2, movement=1)
        my_game.field.add_enemy(my_enemy)
        my_game.fire_towers()
        enemies = my_game.field.get_enemies()
        self.assertEqual(len(enemies), 0)

    def test_reload(self):
        my_game = Round(path)
        my_tower = Tower('john', firing_range=3, damage=1, cooldown=1)
        my_game.field.add_tower(position=(4, 0), tower=my_tower)
        my_enemy = Enemy('bob', health=2, movement=1)
        my_game.field.add_enemy(my_enemy)
        my_game.fire_towers()
        my_game.move_enemies() # enemy at (5, 1)
        my_game.fire_towers() # reloading at this step
        my_game.move_enemies() # enemy at (5, 2)
        my_game.fire_towers() # has to kill
        enemies = my_game.field.get_enemies()
        self.assertEqual(len(enemies), 0)

    def test_range(self):
        my_game = Round(path)
        my_tower = Tower('john', firing_range=3, damage=1, cooldown=1)
        my_game.field.add_tower(position=(4, 0), tower=my_tower)
        my_enemy = Enemy('bob', health=1, movement=1)
        my_game.field.add_enemy(my_enemy)
        my_game.move_enemies()
        my_game.move_enemies()
        my_game.fire_towers()
        enemies = my_game.field.get_enemies()
        self.assertEqual(len(enemies), 0)

    #Tests for enemy initialization
    def test_new_enemy(self):
        my_game = Round(path)
        my_game.enemy_waiting_list = deque([Enemy('alice', 1, 1), None, None, Enemy('bob', 2, 1)])
        my_game.add_possible_new_enemy()
        enemies = my_game.field.get_enemies()
        print(enemies)
        self.assertEqual(enemies[0][1].health, 1)

    def test_round(self):
        my_game = Round(path)
        my_tower = Tower('john', firing_range=9, damage=1, cooldown=2)
        my_game.field.add_tower(position=(4, 0), tower=my_tower)
        my_game.enemy_waiting_list = deque([Enemy('alice', 1, 1), None, None, Enemy("KONO DIO DA", health=9999, movement=1)])
        my_game.run_round()
        self.assertEqual(my_game.pass_damage_dealt(), 1)

if __name__ == '__main__':
    unittest.main()
