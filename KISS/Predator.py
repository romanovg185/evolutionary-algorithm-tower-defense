from KISS.Enemy import Enemy
from KISS.Tower import Tower
from KISS.Round import Round

path = [(5, 0), (5, 1), (5, 2), (5, 3), (5, 4)]

class Predator(object):
    def __init__(self, chromosome):
        s = set()
        for i in range(10):
            for j in range(10):
                s.add((i, j))
        positions_allowed = s.difference(set(path))
        self.placing_matrix = dict(zip(positions_allowed, chromosome))