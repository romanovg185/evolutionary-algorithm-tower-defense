from KISS.Field import Field
import unittest

class FieldTest(unittest.TestCase):
    def test_enemies(self):
        my_field = Field(path=[(0, 0), (0, 1), (1, 1), (2, 1)])
        my_field.add_enemy(42)
        self.assertEqual(my_field.get_enemies[0][1], 42)

    def test_two_enemies(self):
        my_field = Field(path=[(0, 0), (0, 1), (1, 1), (2, 1)])
        my_field.add_enemy(42)
        my_field.add_enemy(9001)
        self.assertEqual(my_field.get_enemies[0][1], 9001)

    def test_towers(self):
        my_field = Field(path=[(0, 0), (0, 1), (1, 1), (2, 1)])
        my_field.add_tower((1, 0), 42)
        self.assertEqual(my_field.get_towers()[0][1], 42)

    def test_two_towers(self):
        my_field = Field(path=[(0, 0), (0, 1), (1, 1), (2, 1)])
        my_field.add_tower((1, 0), 42)
        my_field.add_tower((2, 0), 9001)
        self.assertEqual(my_field.get_towers()[1][1], 9001)

    def test_get_specific_tower(self):
        my_field = Field(path=[(0, 0), (0, 1), (1, 1), (2, 1)])
        my_field.add_tower((1, 0), 42)
        my_field.add_tower((2, 0), 9001)
        self.assertEqual(my_field.get_tower_at((2, 0)), 9001)

if __name__ == '__main__':
    unittest.main()
