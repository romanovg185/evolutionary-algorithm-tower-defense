from KISS.Field import Field
from scipy.spatial.distance import cityblock as dist
from collections import deque

## DEFINING GLOBALS ##

class Round(object):
    def __init__(self, path, enemy_waiting_list=deque()):
        self.field_size = (10, 10)
        self.field = Field(path)
        self.enemy_waiting_list = enemy_waiting_list
        self.damage_dealt = 0 # stores amount of damage dealt this round

    def add_possible_new_enemy(self):
        if len(self.enemy_waiting_list) == 0:
            return
        new_enemy = self.enemy_waiting_list.popleft()
        if new_enemy is not None:
            self.field.add_enemy(new_enemy)

    def move_enemies(self) -> bool:
        "Returns True if enemy deals a point of damage to you"
        damage_flag = False
        enemy_list = self.field.get_enemies()
        for coordinate, enemy in enemy_list:
            index_on_path = [i for i, val in enumerate(self.field.path) if val == coordinate][0] + enemy.movement
            if len(self.field.path) <= index_on_path:
                damage_flag = True
                del(self.field[coordinate])
                continue
            new_position = self.field.path[index_on_path]
            del(self.field[coordinate])
            self.field[new_position] = enemy
        return damage_flag

    def fire_towers(self):
        """Enemies always fire at the enemy closest to the exit"""
        towers = self.field.get_towers()
        enemies = self.field.get_enemies()
        for pos_t, tower in towers:
            for pos_e, enemy in enemies:
                if tower.current_cooldown == 0:
                    if dist(pos_e, pos_t) <= tower.firing_range:
                        enemy.health -= tower.damage
                        if enemy.health <= 0:
                            del(self.field[pos_e])
                        tower.current_cooldown = tower.cooldown
                        break
            else:
                if tower.current_cooldown != 0:
                    tower.current_cooldown -= 1

    def step(self):
        if self.move_enemies():
            self.damage_dealt += 1
        self.fire_towers()
        self.add_possible_new_enemy()

    def run_round(self):
        print("Run start")
        while len(self.field.get_enemies()) > 0 or len(self.enemy_waiting_list) > 0: # while no enemies queued and no enemies alive
            self.step()

    def pass_damage_dealt(self):
        return self.damage_dealt







