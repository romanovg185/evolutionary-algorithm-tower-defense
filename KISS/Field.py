from KISS.Enemy import Enemy
from KISS.Tower import Tower

class Field(dict):
    def __init__(self, path):
        super().__init__(self)
        self.path = path

    def get_enemies(self) -> list:
        "Returns a list of pairs of (coordinate, enemy) for all enemies"
        enemy_list = [(key, val) for key, val in self.items() if key in self.path]
        sorted_enemy_list = []
        for i in self.path:
            for j in enemy_list:
                if j[0] == i:
                    sorted_enemy_list.append(j)
        reversed_sorted_enemy_list = [i for i in reversed(sorted_enemy_list)]
        return reversed_sorted_enemy_list

    def get_towers(self) -> list:
        "Returns all towers and their positions"
        return [(key, val) for key, val in self.items() if key not in self.path]

    def get_tower_at(self, position: tuple) -> Tower:
        """Returns tower at specific position, else returns empty Tower"""
        if position in self.keys():
            return self[position]
        else:
            return Tower(name="Empty")

    def add_enemy(self, enemy: Enemy):
        """Add an enemy to the first point in the path"""
        self[self.path[0]] = enemy

    def add_tower(self, position: tuple, tower: Tower):
        """Add a tower to a position on the field"""
        if position not in self.path:
            self[position] = tower
        else:
            raise ValueError('Cannot place tower on the enemy path')
