class Enemy(object):
    def __init__(self, name: str, health: int, movement: int):
        self.name = name
        self.health = health
        self.movement = movement