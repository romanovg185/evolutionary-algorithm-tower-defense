from KISS.Game import Game
from KISS.Tower import Tower
from KISS.Enemy import Enemy
from KISS.EnemyList import enemy_list
from collections import deque
import unittest

class GameTest(unittest.TestCase):
    def test_game_run(self):
        towers_to_place = [
            ((4, 2), Tower('pew', 2, 1, 1)),
            ((4, 3), Tower('pewtwo', 2, 1, 1))
        ]
        enemies = [
            deque([Enemy('a', 1, 1)]),
            deque([Enemy('a', 1, 1), None, Enemy('b', 4, 1)])
        ]
        my_game = Game(towers_to_place, enemies)
        level_reached = my_game.run()
        self.assertEqual(level_reached, 2)

    def test_tower_memory(self):
        towers_to_place = [
            ((4, 2), Tower('pew', 2, 1, 1)),
            ((9, 3), Tower('pewtwo', 2, 1, 1)),
            ((9, 4), Tower('pewtwee', 2, 1, 1))
        ]
        enemies = [
            deque([Enemy('a', 1, 1)]),
            deque([Enemy('a', 1, 1)]),
            deque([Enemy('a', 1, 1)])
        ]
        my_game = Game(towers_to_place, enemies)
        my_game.life = 1
        level_reached = my_game.run()
        self.assertEqual(level_reached, 2)

    def test_game_death(self):
       towers_to_place = [
           ((4, 2), Tower('pew', 2, 1, 1)),
           ((4, 3), Tower('pewtwo', 2, 1, 1)),
           ((4, 4), Tower('pewtwee', 2, 1, 1)),
           ((4, 5), Tower('pewtwee', 2, 1, 1))
       ]
       enemies = [
           deque([Enemy('a', 1, 1)]),
           deque([Enemy('a', 1, 1), None, Enemy('b', 999, 1)]),
           deque([Enemy('a', 1, 1)]),
           deque([Enemy('a', 1, 1)])
       ]
       my_game = Game(towers_to_place, enemies)
       my_game.life = 1
       level_reached = my_game.run()
       self.assertEqual(level_reached, 2)

    def test_game_one_tower(self):
        towers_to_place = [((4, 0), Tower('pew', 2, 1, 1))]
        my_game = Game(towers_to_place, enemy_list)
        level_reached = my_game.run()
        self.assertEqual(level_reached, 10)
