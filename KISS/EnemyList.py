from collections import deque
from KISS.Enemy import Enemy

small_one = Enemy('Chibi', 1, 1)
medium_one = Enemy('Mr Dood', 3, 1)
large_one = Enemy('Carnage Guy', 9, 1)
colossal_one = Enemy('Collosipede', 27, 1)
fast_one = Enemy('Sanic', 1, 2)
rapid_one = Enemy('Shodow', 3, 3)

enemy_list = [
    deque([small_one]),
    deque([small_one]),
    deque([small_one, small_one, small_one]),
    deque([medium_one]),
    deque([medium_one, small_one, small_one]),
    #deque([fast_one]),
    #deque([medium_one, None, medium_one, None, medium_one]),
    #deque([large_one]),
    #deque([medium_one, large_one]),
    #deque([rapid_one, rapid_one, rapid_one]),
    #deque([colossal_one]),
    #deque([colossal_one, colossal_one, colossal_one, colossal_one, colossal_one]),
    #deque([colossal_one, colossal_one, colossal_one, colossal_one, colossal_one])
]