import numpy as np

from CMC.Path import Path


class Enemy(object):
    def __init__(self, parent_list, health):
        self.path = Path()
        self.position = self.path.pop()
        self.parent_list = parent_list
        self.health = health

    def move(self):
        old_position = (self.position[0], self.position[1])
        self.parent_list.filled_matrix[old_position[0], old_position[1]] = False
        self.position = self.path.pop()
        self.parent_list.filled_matrix[self.position[0], self.position[1]] = True
        self.parent_list.enemies[(self.position[0], self.position[1])] = self
        del(self.parent_list.enemies[(old_position[0], old_position[1])])

    def __lt__(self, other):
        if len(self.path) < len(other.path):
            return True
        return False

class EnemyList(object):
    def __init__(self, N):
        """Filled matrix keeps track of all points that contain an enemy
        Enemies is a dict that maps coordinates to an enemy object"""
        self.filled_matrix = np.zeros((N, N), dtype=bool)
        self.enemies = {}
        self.path = Path()

    def spawn_enemy(self, health):
        """Spawns a new enemy at the beginning of the Path"""
        new_enemy = Enemy(self, health=health)
        new_position = (new_enemy.position[0], new_enemy.position[1])
        self.filled_matrix[new_position[0], new_position[1]] = True
        self.enemies[(new_position[0], new_position[1])] = new_enemy

    def move_all(self):
        """Checks which enemies survived the round and moves those along the Path"""
        older_dict = self.enemies.copy()
        for key, enemy in older_dict.items():
            if enemy.health <= 0.00001:
                del(self.enemies[key])
                self.filled_matrix[key[0], key[1]] = False
        old_dict = self.enemies.copy()
        for key, enemy in old_dict.items():
            enemy.move()