import os
import time
from collections import deque
from multiprocessing import Pool

import matplotlib.pyplot as plt
import numpy as np
from scipy.spatial.distance import cityblock as manhattan_distance

from DLC.Evolver import choose_fittest, mate
from DLC.Enemy import EnemyList
from DLC.Field import Field
from DLC.Path import Path
from DLC.Tower import TowerList

os.system("taskset -p 0xff %d" % os.getpid())


N_individuals = 200
N_squares = 10
N_generations = 3
N_rounds = 28
path = "/home/romano/Desktop/results.txt"

class Session(object):
    def __init__(self, N, p_coordinates = None):
        self.N = N
        self.field = Field(N)
        self.towers = TowerList(N)
        if p_coordinates is None:
            init_list = deque([i for i in range(N**2)])
            np.random.shuffle(init_list)
            self.p_coordinates = init_list
            self.memory_p_coordinates = init_list.copy()
        else:
            self.p_coordinates = p_coordinates
            self.memory_p_coordinates = p_coordinates.copy()
        self.enemy_list = EnemyList(N)
        self.life_remaining = 10

    #Tower selection#
    def choose_coordinate(self):
        """Chooses the coordinate to place a tower based on the p_coordinates matrix"""
        index_chosen = self.p_coordinates.popleft()
        coordinate_chosen = (index_chosen // self.N, index_chosen % self.N)
        return coordinate_chosen

    def is_space_for_tower(self, i, j):
        """Adds a tower if there is space and returns true, otherwise returns false"""
        if self.towers.is_empty_space(i, j):
            self.towers.add_tower(i, j)
            return True
        return False

    def place_tower(self):
        """Places a tower"""
        tower_placed_flag = False
        while not tower_placed_flag:
            (i, j) = self.choose_coordinate()
            tower_placed_flag = self.is_space_for_tower(i, j)

    #Tower fire#
    def fire_towers(self):
        """Fires all towers"""
        for key, tower in self.towers.towers.items():
            coordinate_tower = key
            targets = []
            for enemy_key, enemy in self.enemy_list.enemies.items():
                if manhattan_distance(coordinate_tower, enemy_key) <= tower.range:
                    targets.append(enemy)
            if len(targets) != 0: # makes the tower fire at the enemy closest to the exit
                targets.sort()
                targets[0].health -= tower.damage

    #Enemy regulation#
    def spawn_enemies(self, health):
        """Places a new enemy at the first point of Path"""
        self.enemy_list.spawn_enemy(health=health)

    def move_enemies(self):
        """Moves all enemies"""
        self.enemy_list.move_all()

    #Damage check#
    def get_damage(self):
        """Checks if an enemy has reached the end. If so, remove the enemy from the map and deal
        one damage"""
        final_point = Path().final()
        for key, enemy in self.enemy_list.enemies.items():
            if enemy.position == final_point:
                break
        else:
            return
        del(self.enemy_list.enemies[final_point])
        self.enemy_list.filled_matrix[final_point[0], final_point[1]] = False
        self.life_remaining -= 1

    def update(self):
        self.move_enemies()
        self.fire_towers()
        self.get_damage()

    def round(self, round_info):
        self.place_tower() # Place your one tower per turn
        my_round_info = round_info.split(' ') # Interpret round info/enemy list
        for char in my_round_info: # Update when the enemies spawn
            if char != '-':
                self.spawn_enemies(int(char))
            self.update()
            if self.life_remaining <= 0:
                return False
        while len(self.enemy_list.enemies) != 0: # If all enemies have spawned, keep running until no more enemies
            self.update()
            if self.life_remaining <= 0:
                return False
        return True

    def __str__(self):
        a = []
        for i, x in enumerate(self.towers.filled_matrix):
            for j, y in enumerate(x):
                if y:
                    a.append('t')
                elif self.enemy_list.filled_matrix[i, j]:
                    a.append('e')
                else:
                    a.append(self.field.floor[i][j].icon)
            a.append('\n')
        return_string = ''.join(a)
        return return_string

def run_individual(iterable):
    """The behavior shown by one individual"""
    proc_num = iterable[1]
    next_generation = iterable[0]
    rounds = [
        '1',
        '2',
        '6',
        '10',
        '14',
        '14',
        '18',
        '24',
        '26',
        '30',
        '34',
        '38',
        '42',
        '46',
        '50',
        '68'
    ]
    w = """rounds = [
        '1', #0
        '2',
        '4 1',
        '8',
        '8 - 6 - 1',
        '8 - 6 - 2', #5
        '8 - 6 - 4',
        '8 6', #5
        '10',
        '12 - 3 - 1',
        '14 - 4 - 4',
        '16 10 - 4',
        '18', #10
        '18 - - 8',
        '20 12 8',
        '20 15 8',
        '22 16 10', #15
        '22 18 12',
        '24 20 14',
        '24 12 12 14',
        '26 14 12 18',
        '28 14 16 16',
        '28 14 16 16 16',
        '30 16 16 20 16',
        '32 18 18 22 18',
        '34 20 20 24 20',
        '36 22 22 26 22',
        '40 24 24 28 24'
    ]"""
    np.random.seed(proc_num)
    s = Session(N_squares, next_generation)
    i = 0
    for i, my_round in enumerate(rounds):
        flag = s.round(my_round)
        if not flag:
            break
    return i, s.memory_p_coordinates

def run_generation(new_generation=None):
    print("A non-zero generation has started now")
    if new_generation is None:
        print("First round")
        new_generation = [None for i in range(N_individuals)]
    with Pool(N_individuals) as p:
        my_iterable = [j for j in zip(new_generation,
                                      [i for i in np.random.randint(0, 999999, (N_individuals, 1))])]
        ans = p.map(run_individual, my_iterable)
    scores = [i[0] for i in ans]
    plt.hist(scores, bins=np.arange(0.5, 28.5, 1))
    plt.title("Distribution of fitness scores")
    plt.xlabel("Fitness score")
    plt.ylabel("#individuals")
    plt.show()
    return ans


def parallel_main():
    v = []
    ans = run_generation()
    for i in range(N_generations):
        fittest = choose_fittest(ans)
        print(fittest)
        new_generation = mate(fittest, N_squares, N_individuals)
        newer_generation = new_generation.copy()
        ans = run_generation(newer_generation)

if __name__ == '__main__':
    t = time.time()
    parallel_main()
    print(time.time() - t)