from collections import deque

class Path(object):
    def __init__(self):
        coordinate_list = [
            (5, 0),
            (5, 1),
            (5, 2),
            (5, 3),
            (5, 4),
            (5, 5),
            (5, 6),
            (5, 7),
            (5, 8),
            (5, 9)
        ]
        self.path = deque()
        for coordinate in coordinate_list:
            self.path.append(coordinate)

    def pop(self):
        """Pops a coordinate from the queue"""
        return self.path.popleft()

    def final(self):
        """Gets the final point of the Path"""
        return self.path[-1]

    def first(self):
        """Gets the first point of a Path"""
        return self.path[0]

    def __len__(self):
        return len(self.path)

    def __iter__(self):
        return (i for i in self.path)
