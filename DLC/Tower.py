import numpy as np

from CMC.Path import Path


class Tower(object):
    def __init__(self):
        self.damage = 1
        self.range = 2

class TowerList(object):
    def __init__(self, N):
        self.path = Path()
        self.filled_matrix = np.zeros((N, N), dtype=bool)
        self.towers = {}

    def is_empty_space(self, i, j):
        """Checks if there is space for a tower"""
        if self.filled_matrix[i, j] or (i, j) in self.path:
            return False
        return True

    def add_tower(self, i, j):
        """Adds a tower"""
        self.towers[(i, j)] = Tower()
        self.filled_matrix[i, j] = True
        return True