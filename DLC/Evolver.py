import numpy as np
from more_itertools import unique_everseen
from collections import deque

width_mutability= 20
depth_mutability = 1
mutability_noise = 0.01
winners_reproducing = 20
losers_reproducing = 5

def choose_fittest(generation_result):
    np.random.shuffle(generation_result)
    my_results = sorted(generation_result, key=lambda x: x[0], reverse=True)
    fittest = my_results[:winners_reproducing]
    losers = my_results[winners_reproducing:]
    np.random.shuffle(losers)
    for i in range(losers_reproducing):
        fittest.append(losers.pop())
    return fittest


def old_choose_fittest(generation_result):
    fitness =  [i[0] for i in generation_result]
    max_fitness = max(fitness)
    fittest = []
    losers = []
    # sort into winners and losers
    for el in generation_result:
        if el[0] == max_fitness:
            fittest.append(el)
        else:
            losers.append(el)
    # if too few winners, get a few of the best losers
    if len(fittest) < winners_reproducing:
        np.random.shuffle(losers)
        losers.sort(key=lambda x: x[0])
        while len(fittest) < winners_reproducing:
            fittest.append(losers.pop())
    # if too many winners, make some of them losers
    elif len(fittest) > winners_reproducing:
        np.random.shuffle(fittest)
        while len(fittest) > winners_reproducing:
            losers.append(fittest.pop())
    np.random.shuffle(fittest)
    np.random.shuffle(losers)
    for i in range(losers_reproducing):
        fittest.append(losers.pop())
    np.random.shuffle(fittest)
    return fittest

def add_noise(child):
    random_indices_to_mutate = np.random.choice(np.arange(0, len(child)), width_mutability, replace=False)
    for index in random_indices_to_mutate:
        while index + depth_mutability >= len(child):
            index -= 1
        print(index)
        temp = child[index]
        child[index] = child[index + depth_mutability]
        child[index + depth_mutability] = temp
    return child

def produce_child(parents):
    father = parents[0]
    mother = parents[1]
    inter = [i for i in zip(father, mother)]
    child = []
    for el in inter:
        r = np.random.rand(1)
        if r > 1/2:
            child.append(el[0])
            child.append(el[1])
        else:
            child.append(el[1])
            child.append(el[0])
    child = [i for i in unique_everseen(child)]
    child = add_noise(child)
    return deque(child)


def mate(fittest, N, N_individuals):
    fittest_genes = [i[1] for i in fittest]
    mating_indices = [np.random.choice(len(fittest), 2, replace=False) for i in range(N_individuals)]
    mating_pairs = [(fittest_genes[i[0]], fittest_genes[i[1]]) for i in mating_indices]
    new_generation = [produce_child(pair) for pair in mating_pairs]
    for i in new_generation:
        print(i)
    return new_generation
