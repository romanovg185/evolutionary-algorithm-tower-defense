from CMC.Path import Path

class Field(object):
    def __init__(self, N):
        self.path = Path()
        M = []
        for i in range(N):
            v = []
            for j in range(N):
                if (i, j) in self.path: v.append(Road())
                else: v.append(Plain())
            M.append(v)
        self.floor = M

    def __str__(self):
        a = []
        for i in self.floor:
            for j in i:
                a.append(j.icon)
            a.append('\n')
        s = ''.join(a)
        return s


class Tile(object):
    def __init__(self):
        self.enemy_passable = True
        self.tower_placeable = True
        self.movement_speed = 0

class Road(Tile):
    def __init__(self):
        super().__init__()
        self.tower_placeable = False
        self.movement_speed =  1
        self.icon = '#'

class Plain(Tile):
    def __init__(self):
        super().__init__()
        self.enemy_passable = False
        self.icon = '.'

